
SELECT `customers`.`customerName` FROM `customers`
WHERE `customers`.`country`='Philippines';

SELECT `customers`.`contactLastName`, `customers`.`contactFirstName` FROM `customers`
WHERE `customers`.`customerName`='La Rochelle Gifts';

SELECT `products`.`productName`, `products`.`MSRP` FROM `products`
WHERE `products`.`productName`="The Titanic";

SELECT `employees`.`firstName`, `employees`.`lastName` FROM `employees`
WHERE `employees`.`email`="jfirrelli@classicmodelcars.com";

SELECT `customers`.`customerName` FROM `customers`
WHERE `customers`.`state` IS NULL;

SELECT `employees`.`firstName`, `employees`.`lastName`, `employees`.`email` FROM `employees`
WHERE `employees`.`lastName`="Patterson" AND `employees`.`firstName`="Steve";

SELECT `customers`.`customerName`, `customers`.`country`, `customers`.`creditLimit` FROM `customers`
WHERE `customers`.`country`!="USA" AND `customers`.`creditLimit`>3000;

SELECT `orders`.`customerNumber` FROM `orders`
WHERE `orders`.`comments` LIKE "%DHL%";

SELECT `productlines`.`productLine` FROM `productlines`
WHERE `productlines`.`textDescription` LIKE "%state of the art%";

SELECT DISTINCT `customers`.`country` FROM `customers`;

SELECT DISTINCT `orders`.`status` FROM `orders`;

SELECT `customers`.`customerName`,`customers`.`country` FROM `customers`
WHERE `customers`.`country` IN ("USA", "France", "Canada");

SELECT `employees`.`firstName`,`employees`.`lastName`, `offices`.`city` FROM `employees`
JOIN `offices` ON `offices`.`officeCode`=`employees`.`officeCode`
WHERE `offices`.`city`="Tokyo";

SELECT `customers`.`customerName` FROM `customers`
JOIN `employees` ON `employees`.`employeeNumber`=`customers`.`salesRepEmployeeNumber`
WHERE `employees`.`lastName`="Thompson" AND `employees`.`firstName`="Leslie";

SELECT `products`.`productName`, `customers`.`customerName` FROM `customers`
JOIN `orders` ON `orders`.`customerNumber`=`customers`.`customerNumber`
JOIN `orderdetails` ON `orderdetails`.`orderNumber`=`orders`.`orderNumber`
JOIN `products` ON `products`.`productCode`=`orderdetails`.`productCode`
WHERE `customers`.`customerName`="Baane Mini Imports";

SELECT `employees`.`firstName`, `employees`.`lastName`,`customers`.`customerName`,`offices`.`country` FROM `employees`
JOIN `customers` ON `customers`.`salesRepEmployeeNumber`=`employees`.`employeeNumber`
JOIN `offices` ON `offices`.`officeCode`=`employees`.`officeCode`
WHERE `customers`.`country`=`offices`.`country`;

SELECT `products`.`productName`, `products`.`quantityInStock` FROM `products`
WHERE `products`.`productLine`="Planes" AND `products`.`quantityInStock`<1000;

SELECT `customers`.`customerName` FROM `customers`
WHERE `customers`.`phone` LIKE "%+81%";