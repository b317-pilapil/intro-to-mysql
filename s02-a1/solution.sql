CREATE TABLE `users` (
    `id` INT PRIMARY KEY,
    `email` VARCHAR(100) NOT NULL,
    `password` VARCHAR(300) NOT NULL,
    `datetime_created` DATETIME
);

CREATE TABLE `posts` (
    `id` INT PRIMARY KEY,
    `author_id` INT NOT NULL,
    `title` VARCHAR(300) NOT NULL,
    `content` VARCHAR(5000) NOT NULL,
    `datetime_posted` DATETIME,
    CONSTRAINT `fk_posts_author_id`
        FOREIGN KEY (`author_id`) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);

CREATE TABLE `post_comments` (
    `id` INT PRIMARY KEY,
    `post_id` INT,
    `user_id` INT,
    `content` VARCHAR(5000) NOT NULL,
    `datetime_commented` DATETIME,
    CONSTRAINT `fk_post_comments_user_id`
        FOREIGN KEY (`user_id`) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
    CONSTRAINT `fk_post_comments_post_id`
        FOREIGN KEY (`post_id`) REFERENCES posts(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);

CREATE TABLE `post_likes` (
    `id` INT PRIMARY KEY,
    `post_id` INT,
    `user_id` INT,
    `datetime_liked` DATETIME,
    CONSTRAINT `fk_post_likes_user_id`
        FOREIGN KEY (`user_id`) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
    CONSTRAINT `fk_post_likes_post_id`
        FOREIGN KEY (`post_id`) REFERENCES posts(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);
